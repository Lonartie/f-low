﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F_low.Client
{
    static class Program
    {
        static void Main()
        {
            while (true)
            {
                var connected = false;
                Console.WriteLine("Searching..");
                NetworkController.OnDataReceived_Client += OnDataReceived;
                while (!connected)
                    connected = Task.Run(() => NetworkController.Connect("localhost", 22222)).Result;
                Console.WriteLine("Connected..");
                while (true)
                {
                    string s = Console.ReadLine();
                    NetworkController.Client_Send(s);
                }
            }
        }

        private static void OnDataReceived(object Data)
        {
            Console.WriteLine((string)Data);
        }
    }
}
