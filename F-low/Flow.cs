﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace F_low
{
    public abstract class Flow
    {
        #region Properties
        
        /// <summary>
        /// Public variables that can be synced between clients in the stream
        /// </summary>
        public Dictionary<string, object> PublicVariables = new Dictionary<string, object>();

        /// <summary>
        /// All clients connected to the stream.
        /// </summary>
        public List<FlowClient> AllClients = new List<FlowClient>();

        /// <summary>
        /// Main function running from the start of the client in the stream.
        /// Each client starts when Host-program starts.
        /// </summary>
        // TODO specify return object functionality
        /// <returns>.</returns>
        public abstract object MainExecutor();

        /// <summary>
        /// Function called if an object was sent to this client.
        /// </summary>
        /// <returns>.</returns>
        public abstract object OnDataReceived();

        /// <summary>
        /// Function called if an object was sent to a client in the stream.
        /// </summary>
        /// <returns>.</returns>
        public abstract object OnDataSent();

        /// <summary>
        /// Called if a specific variable was synced by other participants and yourself.
        /// </summary>
        /// <returns>.</returns>
        /// <param name="ID">Identifier for 'PublicVariables'.</param>
        /// <param name="old_one">Old Value.</param>
        /// <param name="new_one">New Value.</param>
        public abstract object OnVariableWasSynced(string ID, object old_one, object new_one);

        /// <summary>
        /// Called if all variables has been synced.
        /// </summary>
        /// <returns>.</returns>
        /// <param name="old_ones">Old ones.</param>
        /// <param name="new_ones">New ones.</param>
        public abstract object OnAllVariablesWasSynced(Dictionary<string, object> old_ones, Dictionary<string, object> new_ones);

        #endregion

        #region Methods

        /// <summary>
        /// Syncs the variable between all participants in the stream.
        /// </summary>
        /// <returns>The variable.</returns>
        /// <param name="ID">Identifier.</param>
        public async Task<bool> SyncVariable(string ID)
        {
            try
            {
                await NetworkController.Client_Send($"Sync:{ID}");
                NetworkController.OnDataReceived_Client += (data) =>
                {
                    var id = ID;
                    PublicVariables[id] = data;
                };

                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// Syncs all the variables with all participants in the stream.
        /// </summary>
        /// <returns>The variables.</returns>
        public async Task SyncVariables()
        {
            await NetworkController.Client_Send($"Sync:All");
            NetworkController.OnDataReceived_Client += (data) =>
            {
                object[] Data = (object[]) data;
                for (int n = 0; n < Data.Length; n += 2)
                    PublicVariables[(string)Data[n]] = Data[n + 1];
            };
        }
    
        /// <summary>
        /// Sends a specific object to a specific client in the stream.
        /// </summary>
        /// <returns>The data.</returns>
        /// <param name="client">Client in the strean.</param>
        /// <param name="Data">Data-object to send to the client.</param>
        /// <param name="Type_ID">Type identifier for data object.</param>
        public async Task SendData (FlowClient client, object Data, string Type_ID)
        {
            NetworkController.Client_Send(client, Data, Type_ID);
        }

        #endregion
    }
}
