﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Net;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading.Tasks;

namespace F_low
{
    public static class NetworkController
    {
        #region Server

        static TcpListener myList = null;
        static List<Socket> clients = new List<Socket>();
        static List<Thread> Receive = new List<Thread>();
        static List<FlowClient> flowClients = new List<FlowClient>();
        static FlowServer flowServer = new FlowServer();
        static Thread Connectioning = null;
        public delegate void ReceiveData(object Data);
        public delegate void ClientConnected(ClientData data);
        public static event ReceiveData OnDataReceived_Server;
        public static event ClientConnected OnClientConnected;

        public static void AddConnection(Socket client)
        {
            clients.Add(client);
            var th = new Thread(() =>
            {
                Socket cl = client;
                while (true)
                {
                    byte[] b = new byte[1000000];
                    try
                    {
                        cl.Receive(b);
                    }
                    catch (SocketException) { Console.WriteLine("Verbindung geschlossen..."); }

                    OnDataReceived_Server?.Invoke(ByteArrayToObject(b));
                }
            });
            Receive.Add(th);
            th.Start();
        }

        public static async Task StartServer(int Port)
        {
            IPAddress ipAd = IPAddress.Parse("0.0.0.0");
            myList = new TcpListener(ipAd, Port);
            myList.Start();
            await OpenConnections();
        }
        
        static async Task OpenConnections()
        {
            await Task.Run(() =>
            {
                while (true)
                {
                    var socket = myList.AcceptSocket();

                    AddConnection(socket);

                    OnClientConnected?.Invoke
                    (
                        new ClientData
                        {
                            IP = (socket.RemoteEndPoint as IPEndPoint).Address.ToString(),
                            ID = clients.Count - 1
                        }
                    );
                }
            });
        }

        public static async Task Server_Send(byte[] data, int ID)
        {
            await Task.Run(() => clients[ID].Send(data));
        }

        public static void Close()
        {
            if (Connectioning != null)
                if (Connectioning.IsAlive)
                    Connectioning.Abort();
            foreach (var receiver in Receive)
                if (receiver != null)
                    if (receiver.IsAlive)
                        receiver.Abort();

            foreach (var client in clients)
                client.Close();
        }

        #endregion

        #region Client

        static TcpClient tcpclnt = null;
        static Thread Receiver;

        public static event ReceiveData OnDataReceived_Client;

        public static async Task<bool> Connect(string ip, int Port)
        {
            try
            {
                tcpclnt = new TcpClient();
                await tcpclnt.ConnectAsync(ip, Port);

                Receiver = new Thread(() =>
                {
                    while (true)
                    {
                        Stream stm = tcpclnt.GetStream();
                        byte[] data = new byte[1000000];
                        stm.Read(data, 0, data.Length);
                        OnDataReceived_Client?.Invoke(ByteArrayToObject(data));
                    }
                });

                Receiver.Start ();

                return true;
            }
            catch (Exception) { return false; }
        }

        public static async Task Client_Send(object Data)
        {
            var data = ObjectToByteArray(Data);
            Stream stm = tcpclnt.GetStream();
            await stm.WriteAsync(data, 0, data.Length);
        }

        public static async Task Client_Send(FlowClient client, object Data, string Type_ID)
        {
            var clientID = ObjectToByteArray(client.ID);
            var data = ObjectToByteArray(Data);
            var typeID = ObjectToByteArray(Type_ID);
            Stream stm = tcpclnt.GetStream();
            await stm.WriteAsync(clientID, 0, clientID.Length);
            await stm.WriteAsync(data, 0, data.Length);
            await stm.WriteAsync(typeID, 0, typeID.Length);
        }


        #endregion

        #region HelperMethods

        public class ClientData
        {
            public string IP;
            public int ID;
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }

        #endregion
    }
}
