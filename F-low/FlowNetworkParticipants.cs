﻿using System;
using System.Collections;
using System.Collections.Generic;
using F_low;

namespace F_low
{
    /// <summary>
    /// Network client connected to the stream
    /// </summary>
    public class FlowClient
    {
        public string Name = "";
        public int? ID;

        public FlowClient(int? ID = null) => 
            this.ID = ID != null ? ID : DateTime.Now.GetHashCode();
    }

    /// <summary>
    /// Keeps track of all properties about server behaviour!
    /// </summary>
    public class FlowServer
    {
        // TODO Eigenschaften, die das Verhalten des Servers verändern!
    }
}