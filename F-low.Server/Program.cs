﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_low.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Server starting..");
            NetworkController.OnClientConnected += new NetworkController.ClientConnected(ClientConnected);
            NetworkController.OnDataReceived_Server += OnDataReceived;
            Task.Run(() => NetworkController.StartServer(22222));
            Console.WriteLine("Done!");
            while (true)
                continue;
        }

        private static void OnDataReceived(object Data)
        {
            Console.WriteLine((string)Data);
        }

        private static void ClientConnected(NetworkController.ClientData data)
        {
            Console.WriteLine($"A new Client has Connected ({data.IP})");
        }
    }
}
